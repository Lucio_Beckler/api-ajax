port = 5001

const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())
app.use(express.json())
app.use(express.static('imagens'))  /* Nome da pasta estática responsável por ter os arquivos */



app.listen(port, ()=>{
    console.log('server iniciado')
})

app.get('/imagens', (req, res)=>{
    res.send({urlImagem: 'http://localhost:5001/imagem1.png'})
})






